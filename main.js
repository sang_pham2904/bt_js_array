function domID(id) {
  return document.getElementById(id);
}
var numArr = [];
console.log("numArr: ", numArr);

function themSo() {
  var soNguyen = domID("nhapSo").value * 1;
  numArr.push(soNguyen);
  console.log("numArr: ", numArr);
  domID("array").innerHTML = `Các số đã nhập vào mảng: ${numArr} `;
}
// Bài 1
function bai1() {
  var i = 0;
  var sum = 0;
  for (i; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      sum += numArr[i];
    }
  }
  domID("ketQua1").innerHTML = `Tổng các số dương là: ${sum}`;
}

// Bài 2
function bai2() {
  var i = 0;
  var count = 0;
  for (i; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      count++;
    }
  }
  domID("ketQua1").innerHTML = `Có tổng cộng ${count} số dương`;
}

// Bài 3
function bai3() {
  var i = 0;
  var min = numArr[0];
  for (i; i < numArr.length; i++) {
    if (min > numArr[i]) {
      min = numArr[i];
    }
  }
  domID("ketQua1").innerHTML = `Số nhỏ nhất là: ${min} `;
}

// Bài 4
function bai4() {
  var i = 0;
  var numDuongArr = [];
  for (i; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      numDuongArr.push(numArr[i]);
    }
  }
  var minDuong = numDuongArr[0];

  if (numDuongArr.length <= 0) {
    minDuong = "Không có số dương trong mảng";
  } else {
    for (var x = 0; x < numDuongArr.length; x++) {
      if (minDuong > numDuongArr[x]) {
        minDuong = numDuongArr[x];
      }
    }
  }
  domID("ketQua1").innerHTML = `Số dương nhỏ nhất là: ${minDuong} `;
}

// Bài 5
function bai5() {
  var i = 0;
  var soChan = 0;
  var numChanArr = [];
  for (i; i < numArr.length; i++) {
    if (numArr[i] % 2 == 0) {
      numChanArr.push(numArr[i]);
    }
  }
  if (numChanArr.length <= 0) {
    soChan = -1;
  } else {
    soChan = numChanArr[numChanArr.length - 1];
  }
  domID("ketQua1").innerHTML = `Số chẵn cuối cùng trong mảng là: ${soChan} `;
}

// Bài 6
function bai6() {
  var viTri1 = domID("doiViTri1").value * 1;
  var viTri2 = domID("doiViTri2").value * 1;
  [numArr[viTri1], numArr[viTri2]] = [numArr[viTri2], numArr[viTri1]];
  domID("ketQua1").innerHTML = `Mảng sau khi đổi : ${numArr} `;
}

// Bài 7
function bai7() {
  for (var i = 0; i < numArr.length; i++) {
    for (var k = i + 1; k < numArr.length; k++) {
      if (numArr[i] > numArr[k]) {
        [numArr[i], numArr[k]] = [numArr[k], numArr[i]];
      }
    }
  }
  domID("ketQua1").innerHTML = `Mảng sau khi sắp xếp : ${numArr} `;
}

// Bài 8
function bai8() {
  function findSNT(num) {
    var checkSNT = true;
    if (num <= 1) {
      checkSNT = false;
    } else if (num > 1) {
      for (var i = 2; i < Math.sqrt(num); i++) {
        if (num % i === 0) {
          checkSNT = false;
          break;
        }
      }
    }
    return checkSNT;
  }
  var soNguyenToDauTien = 0;
  for (var k = 0; k < numArr.length; k++) {
    if (findSNT(numArr[k]) == true) {
      soNguyenToDauTien = numArr[k];
      break;
    }
  }
  console.log("soNguyenToDauTien: ", soNguyenToDauTien);
  domID(
    "ketQua1"
  ).innerHTML = `Số nguyên tố đầu tiên trong mảng là : ${soNguyenToDauTien} `;
}
var o = -101 % 1;
console.log("o: ", o);

// Bài 9
var arrBai9 = [];
function themSoBai9() {
  var nhapSoBai9 = domID("nhapSoBai9").value * 1;
  arrBai9.push(nhapSoBai9);
  console.log("arrBai9: ", arrBai9);
}
function bai9() {
  var demSoNguyen = 0;
  for (var i = 0; i < arrBai9.length; i++) {
    if (arrBai9[i] % 1 == 0) {
      demSoNguyen++;
    }
  }
  domID("ketQua1").innerHTML = `Có ${demSoNguyen} số nguyên trong mảng mới `;
}

// Bài 10
function bai10() {
  var soDuong = 0;
  var soAm = 0;
  var soSanh = "";
  for (var i = 0; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      soDuong++;
    } else if (numArr[i] < 0) {
      soAm++;
    }
  }
  if (soDuong > soAm) {
    soSanh = "Số dương > Số âm";
  } else if (soDuong < soAm) {
    soSanh = "Số dương < Số âm";
  } else if (soDuong == soAm) {
    soSanh = "Số dương = Số âm";
  }
  domID("ketQua1").innerHTML = soSanh;
}
